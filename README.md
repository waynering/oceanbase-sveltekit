# Setting up

To set up the project, you first need a running OceanBase cluster. You have several options for doing so. You can [install OceanBase in your local environment](https://en.oceanbase.com/docs/community-observer-en-10000000000829647), [spin up a virtual machine in the cloud](https://medium.com/oceanbase-database/how-to-install-oceanbase-on-an-aws-ec2-instance-step-by-step-guide-aab852c2e0a7) to run it, or use [OceanBase Cloud in the AWS marketplace](https://aws.amazon.com/marketplace/pp/prodview-d2evwth3ztaja) to set up your cluster in just a few clicks.

In this project, I’m going to use the EC2 approach and the demo server for simplicity. In production, please see OceanBase’s [official guide](https://en.oceanbase.com/docs/community-observer-en-10000000000829659) on deploying in a Kubernetes cluster.

After running the demo server, I created a demo user with the password “demo” that only has access to the `todos` database that I created for this project. 

Here's an example schema for the tasks table:

```sql
CREATE TABLE tasks (
  id INT NOT NULL AUTO_INCREMENT,
  text VARCHAR(255) NOT NULL,
  completed BOOLEAN NOT NULL DEFAULT false,
  PRIMARY KEY (id)
);
```

This creates a tasks table with three columns: `id` (an auto-incrementing integer), `text` (a string of up to 255 characters), and `completed` (a boolean value that defaults to false). The primary key for the table is the `id` column.

## Creating a project
To get started with SvelteKit, you'll need to have Node.js installed on your machine. You can download the latest version of Node.js from the [official website](https://nodejs.org/en).

Once you have Node.js installed, you can create a new SvelteKit project using the following command:

```bash
npx degit sveltejs/kit oceanbase-app
cd oceanbase-app
npm install
```

This will create a new SvelteKit project in the `oceanbase-app` directory and install all the necessary dependencies.

Now, run the following command to start the development server.

```bash
npm run dev
```

This will start the server and allow you to view your app in the browser at `http://localhost:3000`.

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
npm run build
```

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.
