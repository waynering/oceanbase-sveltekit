import mysql from 'mysql2/promise';
import * as dotenv from 'dotenv'; 
dotenv.config();

export const oceanbaseConnection = await mysql.createConnection({
    host: process.env.HOST,
    port: 2881,
    user: process.env.USERNAME,
    database: 'todos',
});
